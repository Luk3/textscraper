package siteScraper;

/*
 * Result
 * 	Holds relative information about a result.
 * 
 * Written by Luke Shiffer.
 */
public class Result {

	// Properties
	private String name;
	private String price;
	
	// Constructor
	public Result(String productName, String productPrice){
		name = productName;
		price = productPrice;
	}
	
	// Product Name only Constructor
	public Result(String productName){
		name = productName;
	}
	
	public String toString(){
		return "Product Name: " + name + "\n\t Product Cost: " + price;
	}
	
// Getters
	public String getName(){
		return name;
	}
	
	public String getPrice(){
		return price;
	}
	
// Setters
	public void setName(String name){
		this.name = name;
	}
	
	public void setPrice(String price){
		this.price = price;
	}
}
