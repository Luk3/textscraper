package siteScraper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/*
 * TextScraper
 * 		INPUT:  Search Term //String
 * 		OUTPUT:  Number of results //Int
 * 
 * 		INPUT:  Search Term and Page Number //String Int
 * 		OUTPUT:  Title/Product Name and Price //String Double
 * 
 * Written by Luke Shiffer.
 * 
 */

public class TextScraper {
	
	final static String URL = "http://www.walmart.com/search/?query=";
	
	/*
	 * main...  Starts and runs the program.
	 */
	public static void main(String[] queries) throws IOException{
		// List of input values
		ArrayList<String> input = new ArrayList<String>();
		
		// Input scanner
		Scanner scanner = new Scanner(System.in);
		
		// Transfer queries to list
		for(String s: queries)
			input.add(s);
		
		// Determine the output while the user wants to continue
		do{
			determineOutput(input);
		} while (goAgain(input, scanner));
		
		System.out.println("Thank you for using TextScraper.  ^o^");
	}
	
	/*
	 * Determine the output based on  queries size.
	 */
	private static void determineOutput(ArrayList<String> queries){
		switch (queries.size()){
		case 1:
			display(getResults(queries.get(0)));
			break;
		case 2:
			display(getResults(queries.get(0), queries.get(1)));
			break;
		default:
			System.out.println("Please enter: 'term' or 'term' and 'page number'...  Thank you.  =D\n eg: \"digital camera\" OR \"digital camera\" 9");
		}
	}
	
	/*
	 * Display the string of number of results. 
	 */
	private static void display(String result) {
		System.out.println(result);
	}

	/*
	 * Display all Result elements in a list.
	 */
	private static void display(ArrayList<Result> results) {
		for (Result r: results)
			System.out.println(r.toString());
	}

	/*
	 * Get input and determine if to run again.
	 */
	private static boolean goAgain(ArrayList<String> queries, Scanner input){
		System.out.print("Enter \"term\" with or without \"integer\" or nothing to quit: ");
		String[] s = input.nextLine().split("\"");
		
		queries.clear();
		
		try{
			queries.add(s[1]);
			queries.add(s[2].substring(1));
			if (!(queries.get(1).length()>0))
				queries.remove(1);}
		catch (Exception e){
			// If user forgot "" or something...
			if (s[0].length()>0)
				return true;
		}
		
		return (queries.size() > 0);
	}
	
	/*
	 * Get document.
	 * 		INPUT:  searchTerm //String
	 * 		OUTPUT:  webpage //Document
	 */
	private static Document getDocument (String searchTerm){
		
		Connection connection = null;
		
		// Attempt to make a connection
		try{
			connection = Jsoup.connect(URL+searchTerm);
			return connection.get();
		} catch (IOException e){
			System.out.println("Error: "+e);
		}
		
		return null;
	}

	/*
	 * Get Results for a Search Term and a Page Number.
	 */
	private static ArrayList<Result> getResults(String searchTerm, String pageNumber) {
		
		// Results
		ArrayList<Result> results = new ArrayList<Result>();
		
		// Attach page number to the search term. 
		Document doc = getDocument(searchTerm+"&page="+pageNumber);
		
		// If a good connection...
		if (doc!=null){			
			// Product titles
			Elements listings = doc.select(".js-product-title");
			// Product Prices
			Elements prices = doc.select(".item-price-container");
			
			// Create a Result for each Product, including name and price of product.
			for (int i=0; i<listings.size() && i<prices.size(); i++)
				results.add(new Result(listings.get(i).text(), prices.get(i).text()));
				
			// Check if price to check if needs to be trimmed.
			for (int i=0; i<results.size(); i++){
				String price = results.get(i).getPrice();
				if (price.indexOf(' ')>-1)
					results.get(i).setPrice(price.substring(0, price.indexOf(' ')));
			}
			
			System.out.println("Findings for ["+searchTerm +"] on page ["+pageNumber+"]:");
		}
		else
			System.out.println("Connection error... 'o'\nPlease try again. Be sure a single space separates the last quotation mark and the page number.");
		
		return results;
	}

	/*
	 * Get Results for a Search Term.
	 * 		INPUT:  Search Term //String
	 */
	private static String getResults(String searchTerm) {
		
		Document doc = getDocument(searchTerm);
		
		// If a good connection...
		if (doc!=null){
			return "Total Results for " + searchTerm + ": " + getTotal(doc.select(".result-summary-container").text());
		}
		
		return null;
	}

	/* 
	 * Get total results of query.
	 * 		INPUT:  result-summary-container class element 
	 * 		OUTPUT:  Total results //int
	 */
	private static String getTotal(String totalResults) {
		return totalResults.split("[ ]")[3];
	}
}
