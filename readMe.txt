
Text Scraper
Written by Luke Shiffer

Description
	Text Scraper can perform two different queries on Wal-Mart's site.
		1)  Total Results for a Search Term
				To use, provide a single search term within quotation marks.
					eg:  "simpsons"
		2)  All Products and Their Cost for a Specified Page Number
				To use, follow 'To use' for 1 and include a number after a single space from the last quotation mark.
					eg:  "simpsons" 4


To Run
	Using a command prompt..
	Navigate to the directory where lukeShiffer_textScraper is in (.../textscraper).
	Type:  java -jar lukeShiffer_textScraper.jar "Search Term" pageNumber
		pageNumber can be excluded. 

Files
	\src\siteScraper
		Result.java
			Holds information about a result.
		TextScraper.java
			Runs the program.

Libraries Used  
	jsoup (Retrivied using maven)

Technical Description
	Program starts with a search term and possibly a page number.
	Those values are stored to a list of strings.

	determineOutput(ArrayList<String>)
		is called to determine the output.
			If one element in the list of strings, a search term...
				Get the appropriate results and display the results.
			If two elements in the list, a search term and a page number...
				Get the appropriate results and display the results.
			Otherwise...
				Provide user with valid input example.

	getResults(String) OR (String, String) 
		is overloaded...
			If only a searchTerm...
				getDocument for the search term.
				If the document exists...
					Return the formatted string with results to be output. 
			If a searchTerm and a pageNumber...
				getDocument for the search term and page number.
				Get all references to the product titles and prices.
				For each element of title and price...
					Add to a list of Result a new Result for the product.
				For each Result in results...
					Check if price needs to be trimmed
						Such as 'Out of stock' or 'Was $x.xx Savings $x.xx'
				Output header.
				Return list of results.

	getDocument(String) 
		takes a string and tries to make a connection....
			If successful connection, returns the document of the connection. 

	getTotal(String)
		Helper function for getResults(String) to trim excess text of container for total results.

	display(String) OR (ArrayList<Result>) 
		is overloaded..
			Will output a string or...
			Will iterate through a list of Results to output. 

	goAgain(ArrayList<String>, Scanner)
		Displays prompt and splits the input by "".
		Clears the list of queries.
		Attempts to add the User's input to the list...
			If there is an exception, will check if the User means to quit or maybe made an input error.
